import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { AppConfig } from '../config/app.config';
import { tokenNotExpired } from 'angular2-jwt';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {
  public token:any;
  constructor( public http: HttpClient,
               public jwtHelper:JwtHelper = new JwtHelper()
               ) { }

  login(user):Observable<any>{
  	return this.http.post(AppConfig.api+'/auth/login', user)
      .pipe(
        tap(res => {
          this.token = res;
          localStorage.setItem("iEmergencias.token", this.token.token)}),
        err => {return err}
      );
  }

  logOut():void {
    if(localStorage.getItem("iEmergencias.token")){
      localStorage.removeItem("iEmergencias.token");
    }
  }  

  isAutheticated():boolean {
    if(localStorage.getItem("iEmergencias.token")){
      return tokenNotExpired("iEmergencias.token");
    }else{
      return false;
    }
  }

  isUser(){
    if(this.isAutheticated()){
      return this.jwtHelper.decodeToken(this.getToken()).roles.indexOf('USER') != -1;
    }else{
      return false;
    }
  }

  isAdmin(){
    if(this.isAutheticated()){
      return this.jwtHelper.decodeToken(this.getToken()).roles.indexOf('ADMIN') != -1;
    }else{
      return false;
    }
  }

  getToken(){
    return localStorage.getItem("iEmergencias.token");
  }

  getDecodeToken(){
    return this.jwtHelper.decodeToken(this.getToken());
  }

  getUserName(){
    if(localStorage.getItem("iEmergencias.token")){
      return this.jwtHelper.decodeToken(this.getToken()).user;
    }
  }

  getIdUser(){
    if(localStorage.getItem("iEmergencias.token")){
      return this.jwtHelper.decodeToken(this.getToken()).sub;
    }
  }
}
