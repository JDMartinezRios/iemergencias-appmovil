import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule }   from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';

import { usuarioService } from '../pages/usuario/usuario.service';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { articulosComponent } from '../pages/articulos/articulos';
import { usuarioComponent } from  '../pages/usuario/usuario';
import { PostService } from '../pages/home/post.service';
import { AuthService } from '../Auth/auth.service';
import { slidesComponent } from '../pages/slides/slides';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName: 'iEmergencias.token',
    tokenGetter: (() => sessionStorage.getItem('token')),
    globalHeaders: [{'Content-Type':'application/json'}],
  }), http, options);
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    articulosComponent,
    usuarioComponent,
    slidesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    articulosComponent,
    usuarioComponent,
    slidesComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PostService,
    File,
    FilePath, 
    FileTransfer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},   
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    JwtHelper,
    AuthService,
    usuarioService,
    Camera]
})
export class AppModule {}
