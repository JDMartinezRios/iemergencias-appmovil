import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { articulosComponent } from '../pages/articulos/articulos';
import { usuarioComponent } from '../pages/usuario/usuario';
import { AuthService } from '../Auth/auth.service';

@Component({
  templateUrl: 'app.html',
  providers:[ AuthService ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = this.authService.isAutheticated() ? usuarioComponent : HomePage;

  pages: Array<{title: string, component: any}>;


  constructor(
   public platform: Platform,
   public statusBar: StatusBar,
   public splashScreen: SplashScreen,
   public authService: AuthService,
   private menu: MenuController,
   private modalCtrl: ModalController) {
    this.menu.enable(false);
    this.initializeApp();

    // used for an example of ngFor and navigation
    // this.pages = [
    //   { title: 'Home', component: HomePage },
    //   { title: 'List', component: ListPage },
    //   { title: 'Articulos', component: articulosComponent},
    //   { title: 'Usuario', component:usuarioComponent}
    // ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();     
    });
  }

  openPage(page: String) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
     if(page === "ListPage"){
      this.nav.setRoot(ListPage);
     }else if(page === "articulosComponent"){
       this.nav.setRoot(articulosComponent);
     }else if(page === "usuarioComponent"){
       this.nav.setRoot(usuarioComponent);
     }
  }

  salir(page){
    this.menu.enable(false);
    this.nav.setRoot(HomePage);
    this.authService.logOut();
  }

  isAuthenticated(){
    return this.authService.isAutheticated();
  }

}
 
