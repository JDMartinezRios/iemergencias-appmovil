import { Component } from '@angular/core';
import { AlertController} from 'ionic-angular';
import { Nav, NavController, Platform } from 'ionic-angular';
import { Loading, LoadingController } from 'ionic-angular';
import { PostService } from './post.service';
import { AppConfig } from '../../config/app.config';
import { AuthService } from '../../Auth/auth.service';
import { usuarioComponent } from '../usuario/usuario';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[ AuthService ]
})
export class HomePage {
  
  public usuarios = {documentNumber: undefined, password: undefined};
  public loading: Loading;
  public show: any;
 
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public postService: PostService,
    public nav: Nav,
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public plt: Platform) {    

  }

  login(){
    this.showLoading()
    this.authService.login(this.usuarios)
    .subscribe(res =>{   
      this.nav.setRoot(usuarioComponent);
    },err =>{
      this.showError('Acceso denegado')
      console.log(err.error);
    })
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere un momento...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

   showError(text) {
    this.loading.dismiss();
 
     let alert = this.alertCtrl.create({
      title: 'Fallo',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

}