import { Component } from '@angular/core';
import { AuthService } from '../../Auth/auth.service';
import { Nav, NavController, ActionSheetController, ToastController, Platform, LoadingController, Loading, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { articulosComponent } from '../articulos/articulos';
import { ListPage } from '../list/list';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { AppConfig } from '../../config/app.config';
import { usuarioService } from './usuario.service';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

declare var cordova: any;

@Component({
	selector:'usuario',
	templateUrl:'usuario.html',
	providers:[ AuthService, usuarioService ]	
})

export class usuarioComponent{	
	public base64Image: any;
	public show: boolean = false;
	public content: String = 'Selecciona una institucion';
	private loading: Loading;
	private lastImage: string = null;
	public notification = {idUsers:undefined, idInstitutions:undefined};
	private idNot: any;	
	public Image: any;
	
	constructor(
		public authService: AuthService,
		public nav: Nav,
		public navCtrl: NavController,
		private cam: Camera,
		private file: File,
		private filePath: FilePath,
		private transfer: FileTransfer,
		private actionSheetCtrl: ActionSheetController,
		private toastCtrl: ToastController,
		private platform: Platform,
		private loadingCtrl: LoadingController,
		private usuarioServ: usuarioService){}

	salir(){
		this.authService.logOut();
		this.nav.setRoot(HomePage);
	}
	presentActionSheet() {
	    let actionSheet = this.actionSheetCtrl.create({
	      title: 'Eliga la opcion',
	      buttons: [
	        {
	          text: 'Suba su foto',
	          handler: () => {
	            this.tomarFoto(this.cam.PictureSourceType.PHOTOLIBRARY);
	          }
	        },
	        {
	          text: 'Usar Camara',
	          handler: () => {
	            this.tomarFoto(this.cam.PictureSourceType.CAMERA);
	          }
	        },
	        {
	          text: 'Cancel',
	          role: 'cancel'
	        }
	      ]
	    });
	    actionSheet.present();
	  }

	tomarFoto(sourceType){
		this.show = true;	
		this.content = 'Envia tu notificacion';
		let  options: CameraOptions = {
			 quality: 100,
			 sourceType: sourceType,
			 destinationType: this.cam.DestinationType.DATA_URL,
			 encodingType: this.cam.EncodingType.JPEG,
			 mediaType: this.cam.MediaType.PICTURE
		};		
		this.cam.getPicture(options).then((imagePath) => {				
		this.base64Image = 'data:image/jpeg;base64,' + imagePath;
		if (this.platform.is('android') && sourceType === this.cam.PictureSourceType.PHOTOLIBRARY) {
		      this.filePath.resolveNativePath(imagePath)
		        .then(filePath => {
		          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
		          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
		          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
		        });
		    } else {
		      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
		      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
		      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
		    }			
		  }, (err) => {
		    this.presentToast('Error mientras seleccionabas la imagen.');
		  });
	}
	
	// Create a new name for the image
	private createFileName() {
	  var d = new Date(),
	  n = d.getTime(),
	  newFileName =  n; //+ ".jpg";
	  return newFileName;
	}
	 
	// Copy the image to a local folder
	private copyFileToLocalDir(namePath, currentName, newFileName) {
	  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
	    this.lastImage = newFileName;
	  }, error => {
	    this.presentToast('Error while storing file.');
	  });
	} 

    presentToast(text) {
	  let toast = this.toastCtrl.create({
	    message: text,
	    duration: 3000,
	    position: 'top'
	  });
	  toast.present();
	}

	// Always get the accurate path to your apps folder
	public pathForImage(img) {
	  if (img === null) {
	    return '';
	  } else {
	    return cordova.file.dataDirectory + img;
	  }
	}

	gotToOtherPage(){
		this.navCtrl.push(articulosComponent);		
	}

	enviarNotificacion(){
		this.notification.idUsers = {
			id:	this.authService.getIdUser()
		};
		this.notification.idInstitutions = {
			id:5
		};	
		this.usuarioServ.enviarNotificacion(this.notification)
		.subscribe(res => {		
			 this.idNot = res;
			 this.enviarFoto(this.idNot.id);
		},err => {		
			alert('ERROR');
		})
	}

	b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

              var blob = new Blob(byteArrays, {type: contentType});
              return blob;
            }

	enviarFoto(id: any){
		var idNoti = id;
		this.show = false;
		this.content = 'Selecciona una institucion';
		var formData = new FormData();				 
		  this.loading = this.loadingCtrl.create({
		    content: 'Subiendo...',
		  });
		  this.loading.present();

		  formData.append("idNotifications", idNoti);
		  formData.append("file",this.Image);

		  this.usuarioServ.subirImagen(formData).subscribe(res => {
		  	this.loading.dismissAll()		    
		    this.base64Image = undefined;
		    this.Image = this.b64toBlob(this.base64Image,'foto/png',512);
		    this.presentToast('Notificacion enviada correctamente');
		  },err => {
		  	this.loading.dismissAll()
		  	this.base64Image = undefined;	
		  	this.presentToast('Notificacion no se enviada correctamente');  
		  })
	}
}