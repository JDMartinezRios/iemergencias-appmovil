import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable} from '@angular/core';
import { AppConfig } from '../../config/app.config';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/Observable/of';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class usuarioService {
	constructor(public http: HttpClient){}

	public enviarNotificacion(noti): Observable <any>{
		return this.http.post(AppConfig.api+'/notifications', noti)
		.pipe(
			tap(res => {return res}),
			err => {return err}
			);
		}

	public subirImagen(imagen):Observable <any>{
		return this.http.post(AppConfig.api + '/upload/notification', imagen)
		.pipe(
			tap(res => {return res}),
			err => {return err}
			);
	}
}